package view;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

import controller.LexicalAnalyzer;
import controller.LexicalError;
import controller.SyntacticAnalyzer;
import controller.Token;

@SuppressWarnings("serial")
public class Main extends JFrame implements KeyListener {
	public static JTextArea text_area;
	private JMenuBar toolbar;
	private JMenu menu_lexical;
	private JMenu menu_sintatic;
	private JMenu menu_test1;
	private JMenu menu_test2;
	private JMenu menu_test3;
	private JScrollPane scroll;
	private JPanel panel, panel_side, panel_message;
	private JTable tabela;
	private DefaultTableModel model;
	private JScrollPane pane;
	private JTextArea text_message;
	private JScrollPane js;
	private LinkedList<Token> list_token;

	public Main() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Main.class.getResource("/com/sun/javafx/scene/web/skin/Strikethrough_16x16_JFX.png")));
		initialize();
	}

	public static void main(String[] args) {
		new Main();
	}

	private void initialize() {
		text_area = new JTextArea();
		scroll = new JScrollPane(text_area);
		setSize(940, 700);
		setTitle("Compilador");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		text_area.addKeyListener(this);
		text_area.setBorder(new Layout());
		panel = new JPanel(new BorderLayout());

		toolbar = new JMenuBar();
		menu_lexical = new JMenu("Léxico");
		menu_sintatic = new JMenu("Sintático");
		menu_test1 = new JMenu("Teste 1");
		menu_test2 = new JMenu("Teste 2");
		menu_test3 = new JMenu("Teste 3");

		setJMenuBar(toolbar);
		toolbar.add(menu_lexical);
		toolbar.add(menu_sintatic);
		toolbar.add(menu_test1);
		toolbar.add(menu_test2);
		toolbar.add(menu_test3);

		action_menu();

		panel_message = new JPanel(new BorderLayout());
		panel_message.setBorder(BorderFactory.createTitledBorder("Mensagens"));
		text_message = new JTextArea(20, 30);
		text_message.setEditable(false);
		js = new JScrollPane(text_message);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		panel_side = new JPanel(new BorderLayout());
		model = new DefaultTableModel(new String[][] {}, new String[] { "Codigo", "Token", "Descricao" });
		tabela = new JTable(model);
		tabela.setEnabled(false);

		pane = new JScrollPane(tabela);

		panel_side.add(scroll, BorderLayout.CENTER);
		panel_side.add(pane, BorderLayout.EAST);
		panel.add(panel_side, BorderLayout.CENTER);
		panel_message.add(js, BorderLayout.CENTER);
		panel.add(panel_message, BorderLayout.SOUTH);

		setContentPane(panel);
		setVisible(true);
	}

	/**
	 * TECLAS DE ATALHO
	 * 
	 * CTRL + R = Run CTRL + Q = Exit CTRL + 1 = Test 1 CTRL + 2 = Test 2
	 */
	@SuppressWarnings("static-access")
	public void keyPressed(KeyEvent event) {
		if (event.isControlDown()) {

			if (event.getKeyCode() == event.VK_R)
				process_analysis();

			if (event.getKeyCode() == event.VK_1)
				text_area.setText(test1());

			if (event.getKeyCode() == event.VK_2)
				text_area.setText(test2());

			if (event.getKeyCode() == event.VK_3)
				text_area.setText(test3());

			if (event.getKeyCode() == event.VK_Q)
				System.exit(0);
		}
	}

	/**
	 * REALIZA O PRE PROCESSAMENTO DAS INFORMACOES E EXIBE O PARECER DA ANALISE
	 */
	private void process_analysis() {

		// CAPTURA OS TOKENS INFORMADOS
		String code = text_area.getText() + "\n";

		// REALIZA A ANALISE
		list_token = new LinkedList<Token>();
		LexicalAnalyzer analyzer = new LexicalAnalyzer(code);
		tabela.setModel(model);

		// LIMPA A TABELA
		while (tabela.getRowCount() > 0) {
			model.removeRow(0);
		}

		Token t;

		// INSERE OS TOKENS ENCONTRADOS NA TABELA DE RESULTADO
		try {
			while ((t = analyzer.nextToken()) != null) {
				list_token.add(t);
				model.addRow(t.toArray());
			}
			text_message.setText("Análise léxica concluída com sucesso");
		} catch (LexicalError e) {
			if (e.isComment()) {
				int res = text_area.getText().lastIndexOf("(*");
				text_area.select(res, res + e.getIndex());
			} else if (e.isLiteral()) {
				int res = text_area.getText().lastIndexOf('"');
				text_area.select(res, res + e.getIndex());
			} else {
				text_area.select(e.getIndex(), e.getIndex() + e.getLenght());
			}
			text_message.setText(e.getMessage());
		}
	}

	private void sintatic_analysis() {

		SyntacticAnalyzer s = new SyntacticAnalyzer(listTokens());
		text_message.setText(s.analize());

	}

	private ArrayList<Token> listTokens() {

		ArrayList<Token> tmp = new ArrayList<Token>();

		for (Token t : list_token)
			tmp.add(t);

		return tmp;
	}

	/**
	 * DEFINE AS ACOES DOS MENUS
	 */
	public void action_menu() {

		menu_lexical.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				process_analysis();
			}
		});

		menu_sintatic.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				process_analysis();
				sintatic_analysis();
			}
		});

		menu_test1.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				text_area.setText(test1());
			}
		});

		menu_test2.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				text_area.setText(test2());
			}
		});

		menu_test3.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				text_area.setText(test3());
			}
		});
	}

	public String test1() {
		return    "program Teste17;\r\n" 
				+ "begin\r\n"
				+ "    writeln(\"Hello\");\r\n" 
				+ "    writeln(\"Its me\");\r\n"  
				+ "end.\r\n";
	}

	public String test2() {
		return    "program Teste15;\r\n" 
				+ "var\r\n"
				+ "    i: integer;\r\n"
				+ "begin\r\n"
				+ "	for i := 1 to 10 do\r\n" 
				+ "	begin\r\n"  
				+ "		writeln(20);\r\n"  
				+ "	end;\r\n"
				+ "end.\r\n";
	}

	public String test3() {
		return    "program teste20;\r\n" 
				+ "procedure print(a,b:integer);\r\n"
				+ "var i, j : integer;\r\n"
				+ "begin\r\n"
				+ "    writeln(77);\r\n" 
				+ "end;\r\n"
				+ "begin\r\n"
				+ "    call print(11,12);\r\n"
				+ "end.\r\n";
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
	}

}