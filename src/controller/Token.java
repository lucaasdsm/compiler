package controller;

/**
 * CLASSE RESPONSAVEL POR IDENTIFICAR UM TOKEN ENCONTRADO NO CODIGO.
 * AO FINAL DO PROCESSAMENTO, TODOS OS TOKENS SAO INFORMADOS NA TABELA DE RESULTADO
 */
public class Token {
	String desc, name;
	int code;
	
	boolean reserved_word;
	
	public Token(String name, int code, String desc) {
		super();
		this.name = name;
		this.code = code;
		this.desc = desc;
	}
	
	public Token(Token t) {
		this.code = t.getCode();
		this.desc = t.getDesc();
		this.name = t.getName();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public Integer getValue(){
		return Integer.parseInt(this.getName());
	}

	public void setReserved_word(boolean reserved_word) {
		this.reserved_word = reserved_word;
	}
	public String toString(){
		return name + " | " + code + " | " + desc;
	}
	
	public String[] toArray(){
		return new String[]{code+"", name, desc};
	}
	
	public Token clone(){
		return new Token(name,code,desc);
	}
	
}
